package openthinks.others.webpages.additional;

import com.gargoylesoftware.htmlunit.html.HtmlElement;

public interface AdditionalProcessor {

	void process(HtmlElement element);

}
